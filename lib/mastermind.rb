class Code
  attr_reader :pegs

  PEGS = {
    "R" => "red",
    "G" => "green",
    "B" => "blue",
    "Y" => "yellow",
    "O" => "orange",
    "P" => "purple"
  }

  def initialize(array)
    @pegs = array
  end

  def self.parse(string)
    pegs = string.upcase.chars
    pegs.each { |color| raise "Invalid peg color" unless PEGS[color] }
    Code.new(pegs)
  end

  def self.random
    pegs = PEGS.map { |key, value| key }.shuffle
    pegs = pegs[0..3]
    Code.new(pegs)
  end

  def [](idx)
    pegs[idx]
  end

  def exact_matches(guess)
    matches = 0
    self.pegs.each_index do |peg|
      matches += 1 if self.pegs[peg] == guess.pegs[peg]
    end

    matches
  end

  def near_matches(guess)
    close_match = 0
    colors = []
    guess.pegs.each do |color|
      if self.pegs.include?(color)
        close_match += 1 unless colors.include?(color)
        colors << color
      end
    end

    close_match - exact_matches(guess)
  end

  def ==(guess)
    return false unless guess.is_a?(Code)
    exact_matches(guess) == 4
  end

end

class Game
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def get_guess
    puts "Please take a guess. (Ex. RBYG)"
    Code.parse(gets.chomp)
  end

  def display_matches(guess)
    exact_matches = @secret_code.exact_matches(guess)
    near_matches = @secret_code.near_matches(guess)
    print "Your exact matches: #{exact_matches}\n"
    print "Your near matches: #{near_matches}"
  end

end
